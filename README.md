# Lodash vs. native js
Quick overview of performance and usability testing of lodash vs. native javascript functions 

## Dataset
Dataset randomly generated consisting of 5000 items
```
var data = []
for (var i = 0; i < 5000; i++) {
    data.push({
        name: uuid.v4(),
        number: Math.random() * 1000,
    })
}
```
## 1. Benchmarks

### Find
-   #### map-native    
	`data.find(function (o) {return o.name[0] === '0' && o.number > 990})`
    
-   #### map-lodash
	`_.find(data, function (o) {return o.name[0] === '0' && o.number > 990})`

https://measurethat.net/Benchmarks/ShowResult/126683

### Map
-   #### map-native    
	`data.map(function (o) {return o.name + o.number.toString()})`
    
-   #### map-lodash
	`_.map(data, function (o) {return o.name + o.number.toString()})`

https://measurethat.net/Benchmarks/ShowResult/126670

### Filter
-   #### filter-native    
	`data.filter(function (o) {return o.name[0] === '0' && o.number > 990})`
    
-   #### filter-lodash
	`_.filter(data, function (o) {return o.name[0] === '0' && o.number > 990})`

https://measurethat.net/Benchmarks/ShowResult/126685

### Reduce
-   #### reduce-native    
	`data.reduce(function (acc, o) {return o.name + acc})`
    
-   #### reduce-lodash
	`_.reduce(data, function (acc, o) {return o.name + acc}, '')`

https://measurethat.net/Benchmarks/ShowResult/126692


